Source: visual-regexp-el
Section: lisp
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Sean Whitton <spwhitton@spwhitton.name>
Build-Depends: debhelper (>= 10),
 dh-elpa
Standards-Version: 4.1.4
Vcs-Browser: https://salsa.debian.org/emacsen-team/visual-regexp-el
Vcs-Git: https://salsa.debian.org/emacsen-team/visual-regexp-el.git
Homepage: https://github.com/benma/visual-regexp.el/

Package: elpa-visual-regexp
Architecture: all
Depends: ${elpa:Depends}, ${misc:Depends},
 emacsen-common
Recommends: emacs (>= 46.0)
Enhances: emacs
Description: in-buffer visual feedback while using Emacs regexps
 visual-regexp for Emacs is like `replace-regexp`, but with live
 visual feedback directly in the buffer.
 .
 While constructing the regexp in the minibuffer, you get live visual
 feedback for the matches, including group matches. While constructing
 the replacement in the minibuffer, you get live visual feedback for
 the replacements.
 .
 It can be used to replace all matches in one go (like
 `replace-regexp`), or a decision can be made on each match (like
 `query-replace-regexp`).
